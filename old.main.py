import sys
sys.path.append('/usr/local/lib/python2.7/dist-packages')
import os
import nltk
import json
from nltk.parse import stanford as stp
from nltk.corpus import wordnet as wn
os.environ['STANFORD_PARSER'] = '/home/diazharizky/stanford-parser-full-2018-02-27/stanford-parser.jar'
os.environ['STANFORD_MODELS'] = '/home/diazharizky/stanford-parser-full-2018-02-27/stanford-parser-3.9.1-models.jar'
parser = stp.StanfordDependencyParser(model_path='englishPCFG.ser.gz')


def amodChecker(sen):
    res = []
    for i, val in enumerate(sen):
        if val[1] == "amod":
            if val[0][1] == "NN" and val[2][1] == "JJ":
                res = [str(val[2][0]), str(val[2][1]), 'amod', str(val[0][0]), str(val[0][1])]
    return res


def dobjChecker(sen):
    res = []
    a = -1
    b = -1
    for i, val in enumerate(sen):
        if val[1] == "nsubj":
            if val[0][1] == "VBD" and val[2][1] == "NN":
                a = i
        if val[1] == "dobj":
            if val[0][1] == "VBD" and val[2][1] == "JJ":
                b = i
    if a > -1 and b > -1:
        if sen[a][0] == sen[b][0]:
            res = [str(sen[b][2][0]), 'dobj', str(sen[a][2][0])]
    return res


def acompChecker(sen):
    res = []
    a = -1
    b = -1
    for i, val in enumerate(sen):
        if val[1] == "nsubj":
            a = i
        if val[1] == "xcomp":
            b = i
    if a > -1 and b > -1:
        if sen[a][0] == sen[b][0]:
            res = [str(sen[b][2][0]), 'acomp', str(sen[a][2][0])]
    return res


def copChecker(sen):
    res = []
    a = -1
    b = -1
    for i, val in enumerate(sen):
        if val[1] == "nsubj":
            a = i
        if val[1] == "cop":
            b = i
    if a > -1 and b > -1:
        if sen[a][0] == sen[b][0]:
            res = [str(sen[b][0][0]), str(sen[b][0][1]), 'cop', str(sen[a][2][0]), str(sen[a][2][1])]
    return res


def advmodChecker(sen):
    res = []
    a = -1
    b = -1
    for i, val in enumerate(sen):
        if val[1] == "nsubjpass":
            a = i
        if val[1] == "advmod":
            b = i
    if a > -1 and b > -1:
        if sen[a][0] == sen[b][0]:
            res = [str(sen[b][2][0]), 'advmod', str(sen[a][2][0])]
    return res


with open('train-data.txt') as tr:
    with open('result.txt', 'a') as hs:
        for i, cnt in enumerate(tr.readlines()):
            for parse in parser.raw_parse(cnt):
                tre = list(parse.triples())
                tmp_dob = dobjChecker(tre)
                tmp_amo = amodChecker(tre)
                tmp_aco = acompChecker(tre)
                tmp_adv = advmodChecker(tre)
                tmp_cop = copChecker(tre)
                if len(tmp_dob) > 0:
                    tmp_dob.append(i)
                    hs.write(json.dumps(tmp_dob) + '\n')
                if len(tmp_amo) > 0:
                    tmp_amo.append(i)
                    hs.write(json.dumps(tmp_amo) + '\n')
                if len(tmp_aco) > 0:
                    tmp_aco.append(i)
                    hs.write(json.dumps(tmp_aco) + '\n')
                if len(tmp_adv) > 0:
                    tmp_adv.append(i)
                    hs.write(json.dumps(tmp_adv) + '\n')
                if len(tmp_cop) > 0:
                    tmp_cop.append(i)
                    hs.write(json.dumps(tmp_cop) + '\n')

# result = []
# with open('result.txt') as hs:
#     result = hs.readlines()
# with open('train-data.txt') as tr:
#     trainData = tr.readlines()
# with open('smoother-result.txt', 'a') as w:
#     for i, cnt in enumerate(trainData):
#         for j, rez in enumerate(result):
#             res = json.loads(rez.replace('\n', ''))
#             if res[2] == i:
#                 for parse in parser.raw_parse(cnt):
#                     tree = list(parse.triples())
#                     for tre in tree:
#                         if tre[1] == "compound":
#                             if res[1] == tre[0][0] or res[1] == tre[2][0]:
#                                 res[1] = str(tre[2][0]) + "compound" + res[1]
#                         if tre[1] == "advmod":
#                             if res[0] == tre[0][0] or res[1] == tre[2][0]:
#                                 res[0] = str(tre[2][0]) + "advmod" + res[0]
#                         if tre[1] == "neg":
#                             if (tre[2][0] == "no"):
#                                 if res[1] == tre[0][0] or res[1] == tre[2][0]:
#                                     res[1] = "no " + res[1]
#                             else:
#                                 if res[0] == tre[0][0] or res[1] == tre[2][0]:
#                                     res[0] = "not " + res[0]
#                 w.write(json.dumps(res) + '\n')
