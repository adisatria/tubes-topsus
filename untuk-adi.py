import json

dataset = []
result = []
# with open('clean-dataset.txt') as ds, open('smoother-result.txt') as rs:
with open('pres_proc.txt') as ds, open('pres_smoother-result.txt') as rs:
    dataset = ds.readlines()
    result = rs.readlines()
    
# with open('final.txt', 'w') as w:
with open('pres_final.txt', 'w') as w:
    for i, cnt in enumerate(dataset):
        elm = []
        for j in result:
            tmp = json.loads(j)
            if i == tmp[2]:
                w.write(json.dumps([str(tmp[0]), str(tmp[1])]))
        w.write('\n')